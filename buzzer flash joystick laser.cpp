// Arduino pin numbers
const int SW_pin = 2; // digital pin connected to switch output
const int X_pin = 0; // analog pin connected to X output
const int Y_pin = 1; // analog pin connected to Y output
#define BUZZER_PIN  3
int laserPin = 10;

void setup() {
  pinMode(SW_pin, INPUT);
  digitalWrite(SW_pin, HIGH);
  pinMode(BUZZER_PIN, OUTPUT);
  Serial.begin(115200);
  pinMode (13, OUTPUT);
  pinMode (laserPin, OUTPUT);
}

void loop() {
  /*Serial.print("Switch:  ");
  Serial.print(digitalRead(SW_pin));
  Serial.print("\n");
  Serial.print("X-axis: ");
  Serial.print(analogRead(X_pin));
  Serial.print("\n");
  Serial.print("Y-axis: ");
  Serial.println(analogRead(Y_pin));
  Serial.print("\n\n");
  delay(500);*/
  
  //7color flash
  if(analogRead(X_pin) < 100 && analogRead(Y_pin) < 100 && digitalRead(SW_pin) == 1)
  {
  digitalWrite (13, HIGH); // set the LED on
  delay (500); // wait for a second
  digitalWrite (13, LOW); // set the LED off
  delay (500);
  }
  
//buzzer when joystick pressed
 if(digitalRead(SW_pin) == 0)
  {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(5);
  digitalWrite(BUZZER_PIN, LOW);
  delay(5);
  
 }
 
//laser
  if(analogRead(X_pin) > 900 && analogRead(Y_pin) > 900 && digitalRead(SW_pin) == 1)
  {
  digitalWrite (laserPin, HIGH); // Turn Laser On
   delay (1000); // On For Half a Second
   digitalWrite (laserPin, LOW); // Turn Laser Off
   delay (500); // Off for half a second
  }
    
  delay(0);
  
}

